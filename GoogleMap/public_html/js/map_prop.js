var map;
var marker;
var myCenter = new google.maps.LatLng(51.508742, -0.120850);
var geocoder = new google.maps.Geocoder();

function initialize() {    //display the starting image for the google map
    var mapOptions = {
        zoom: 10, // bydefault zoom is 10 set
        center: myCenter
    };
    map = new google.maps.Map(document.getElementById('map_canvas'),mapOptions);
    marker = new google.maps.Marker({position: myCenter});
    google.maps.event.addListener(map, 'click', function (event) {
        marker.setMap(null);
        getAddress(event.latLng,1);     
    });
}
function showList()
{
   var markers=[];
   var marker,i=0;
    var searchBox = new google.maps.places.SearchBox(document.getElementById("pac_input"));
     google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();
    if (places.length == 0) {
      return;
    }
    for ( i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }
     // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
 // Create a marker for each place.
     marker = new google.maps.Marker({
        map: map,
        title: place.name,
        position: place.geometry.location,
       //animation: google.maps.Animation.BOUNCE
      });

      markers.push(marker);
      bounds.extend(place.geometry.location);
      mycenter=place.geometry.location;
          marker.setMap(null);
      getAddress(mycenter);
      marker.setMap(null);
      
    }
    map.fitBounds(bounds);
    map.setZoom(10);
  });
 // Bias the SearchBox results towards places that are within the bounds of the
  // current map's viewport.
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });
}
function getAddress(location,flag)  // for get the latitiude and longtiude function and get the address the given location
{
    var lat = parseFloat(location.lat());
    var lng = parseFloat(location.lng());
    document.getElementById("set_lat").innerHTML = "Latitude:  " + lat;
    document.getElementById("set_lag").innerHTML = "Longitude:  " + lng;
    var latlng = new google.maps.LatLng(lat, lng);
     marker = new google.maps.Marker({position: latlng});
      marker.setMap(map);
    geocoder.geocode({'latLng': latlng}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
                document.getElementById("set_address").innerHTML = "Location: " + results[1].formatted_address;
            }
            if(flag==1)
            {
                document.getElementById("pac_input").value =  results[1].formatted_address;  
            }
        }
        flag=0;
    });
    }
    
google.maps.event.addDomListener(window, 'load', initialize);     //inilaitize load the function